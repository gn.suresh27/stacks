resource "aws_instance" "myec2instance" {
  ami ="ami-04902260ca3d33422"
  instance_type = "t2.micro"
  user_data = file("${path.module}/app1-install.sh")
  tags = {
      "Name" = "ec2 demo"
  } 
}
